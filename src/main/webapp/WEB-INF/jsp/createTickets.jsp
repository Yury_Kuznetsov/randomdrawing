<%@taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">
    <header class="d-flex flex-column flex-md-row align-items-center p-3 px-md-4 mb-3 bg-white border-bottom shadow-sm">
    </header>
    <title>Generate</title>
    <style>

    </style>
</head>
<body>

<form:form id="generate" modelAttribute="tickets" action="/generateTickets" method="post">
    <table align="center">
        <tr>
            <div align="center">
                <form:label path="amount">Amount</form:label>
                <form:input path="amount" type="number" value=""/>
            </div>
            <div align="center">
            <form:label path="wins">Wins</form:label>
            <form:input path="wins" type="number" value=""/>
            </div align="center">
            <div align="center">
            <form:button id="create" name="create">generate winners</form:button>
            </div>
        </tr>
    </table>
</form:form>
</body>