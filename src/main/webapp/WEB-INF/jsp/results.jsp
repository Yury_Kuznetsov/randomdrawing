<%@ page language="java" contentType="text/html; charset=cp1251"
         pageEncoding="ISO-8859-1" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">
    <header class="d-flex flex-column flex-md-row align-items-center p-3 px-md-4 mb-3 bg-white border-bottom shadow-sm">
        </nav>
    </header>
    <title>Winners</title>
</head>
<body>
<table border="1" align="center">
    <tr style="font-size: 10">
        <td>Win tickets</td>
    </tr>
    <c:forEach var="ticket" items="${ticket}">
        <tr style="font-size: 10">
            <td align="center">${ticket}</td>
        </tr>
    </c:forEach>
</table>
</body>
</html>