package by.random.controller;

import by.random.model.Ticket;
import by.random.repository.RandomDrawingRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

@Controller
@RestController
@RequestMapping("")
public class BaseController {
    @Autowired
    private RandomDrawingRepository randomDrawingRepository;

    @RequestMapping(value = "", method = RequestMethod.GET)
    public ModelAndView home() {
        ModelAndView mav = new ModelAndView();
        mav.setViewName("Base");
        return mav;
    }

    @RequestMapping(value = "/generate", method = RequestMethod.GET)
    public ModelAndView createTicketForm() {
        ModelAndView mav = new ModelAndView();
        mav.addObject("tickets", new Ticket());
        mav.setViewName("createTickets");
        return mav;
    }

    @PostMapping("/generateTickets")
    public ModelAndView submitTickets(@ModelAttribute("tickets") Ticket ticket) {
        ModelAndView mav = new ModelAndView();
        mav.addObject("ticket", randomDrawingRepository.generateList(ticket.getAmount(), ticket.getWins()));
        mav.setViewName("results");
        return mav;
    }
}
