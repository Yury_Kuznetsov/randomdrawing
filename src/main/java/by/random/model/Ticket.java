package by.random.model;

public class Ticket {
    private Integer amount;
    private Integer wins;

    public Ticket(Integer amount, Integer wins) {
        this.amount = amount;
        this.wins = wins;
    }

    public Ticket() {

    }

    public Integer getAmount() {
        return amount;
    }

    public void setAmount(Integer amount) {
        this.amount = amount;
    }

    public Integer getWins() {
        return wins;
    }

    public void setWins(Integer wins) {
        this.wins = wins;
    }
}
