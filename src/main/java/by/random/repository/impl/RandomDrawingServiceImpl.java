package by.random.repository.impl;

import by.random.repository.RandomDrawingRepository;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

@Component
public class RandomDrawingServiceImpl implements RandomDrawingRepository {
    @Override
    public List generateList(int amount, int wins) {
        List<Number> list = IntStream.range(1, amount)
                .boxed()
                .collect(Collectors.toList());
        Collections.shuffle(list);
        List<Number> win = new ArrayList<>();
        for(int i = 0; i < wins; i++) {
            win.add(list.get(i));
        }
        return win;
    }
}
