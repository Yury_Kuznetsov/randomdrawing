package by.random.repository;

import by.random.model.Ticket;

import java.util.List;

public interface RandomDrawingRepository {
    List generateList(int number, int wins);
}
